README
======

Installation
------------

`git clone https://gitlab.com/sgur/powershellrc.git ~/Documents/WindowsPowerShell`

### Profile.ps1

```powershell
Join-Path (Split-Path $Profile -Parent) Common | Get-ChildItem -Filter *.ps1 | ForEach {. $_.FullName}
```

### Microsof.PowerShell\_profile.ps1

```powershell
Join-Path (Split-Path $Profile -Parent) Console | Get-ChildItem -Filter *.ps1 | ForEach {. $_.FullName}
```

Customization
-------------

### Microsof.PowerShell\_profile.ps1

```powershell
# PsReadLine (optional)
Import-Module PSReadline

if ((Get-Module -ListAvailable -Name PSReadLine).Count -gt 0)
{
	Set-PSReadlineOption -EditMode Emacs
	Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
	Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

	Set-PSReadlineOption -AddToHistoryHandler {
		param($Command)
			$LastCommand = $(cat $(Get-PSReadlineOption).HistorySavePath -Tail $([regex]::Matches($Command, '\n').Count + 1) -Encoding UTF8) -join ''
			return $LastCommand -ne ($Command -replace '\n','`')
	}
}
```
