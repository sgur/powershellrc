Set-Alias -Name ph -Value Peco-History
Set-Alias -Name pp -Value Peco-Process

Set-Alias -Name pr -Value Invoke-PecoHistory
Set-Alias -Name pkill -Value Kill-PecoProcess

function Invoke-PecoHistory {
	Peco-History | Invoke-History
}

function Stop-PecoProcess {
	Peco-Process | Stop-Process
}

function Peco-History {
	$id = Get-History `
		| Sort-Object -Unique -Property CommandLine `
		| Sort-Object -Property Id `
		| ForEach-Object {$_.CommandLine+"`0"+$_.Id} `
		| peco --null
	if ($id -ne $null) {
		Get-History -Id $id
	}
	else { @() }
}

function Peco-Process {
	$id = Get-Process `
		| ForEach-Object `
		  {"{0:0000#}:{1} {2}`0{3}" -f $_.Id, $_.ProcessName, $_.Path, $_.Id} `
		| peco --null
	if ($id -ne $null) {
		Get-Process -Id $id
	}
	else { @() }
}

