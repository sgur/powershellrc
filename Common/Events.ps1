
function Get-LogonEvent {
	Get-EventLog -LogName System -After (Get-Date).AddHours(-24) |Where Source -eq "Microsoft-WIndows-Winlogon"
}

function Get-WinLogonEvent {
	Get-WinEvent -LogName System -MaxEvents 300 `
		| Where Id -in 7002,7001 `
		| Format-List -Property Id,TimeCreated,Message,ProviderName
}
