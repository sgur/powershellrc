# https://github.com/staxmanade/DevMachineSetup/blob/master/GlobalScripts/Change-Directory.ps1

Set-Alias -Name cdq -Value Set-GhqLocation
Set-Alias -Name lsq -Value Get-GhqListEntry

function Set-GhqLocation {
	$list = ghq list --full-path $args
	if ($list.Count -eq 1) {
		cd ($list)
	}
	elseif ($list.Count -gt 1) {
		$item = ($list | peco)
		if ($item.Length -ne "") {
			cd $item
		}
	}
}

function Get-GhqListEntry {
	[CmdletBinding()]
	param([Parameter(ValueFromPipeline=$true)][string[]]$InputString,
			[Parameter(Mandatory=$false)][switch]$FullPath)
	begin {
		$param = ""
		if ($FullPath) {
			$param = "--full-path"
		}
	}
	process {
		if (!$InputString.Length) {
			@(ghq list $param | peco)
		}
		else {
			$var = @()
			foreach ($s in $InputString) {
				$var += ghq list $param $InputString
			}
			$var
		}
	}
}

