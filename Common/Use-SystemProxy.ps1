if ((Get-WmiObject Win32_ComputerSystem).PartOfDomain)
{
	# WebClient via Proxy with Auth
	$proxy = [System.Net.WebRequest]::GetSystemWebProxy()
	$proxy.Credentials = [System.Net.CredentialCache]::DefaultCredentials

	[System.Net.WebRequest]::DefaultWebProxy = $proxy
}
