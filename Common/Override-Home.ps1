if ((Get-WmiObject Win32_ComputerSystem).PartOfDomain)
{
	# Home
	$HOMEDRIVE = "C:\"
	$HOMEPATH = "Users\" + $Env:USERNAME

	# Set and force overwrite of the $HOME variable
	Set-Variable HOME "$HOMEDRIVE$HOMEPATH" -Force

	# Set the "~" shortcut value for the FileSystem provider
	(Get-PsProvider 'FileSystem').Home = $HOMEDRIVE + $HOMEPATH
}
