# Git 管理ディレクトリ
function git_branch {
	if (Test-Path .git) {
		git branch 2>$null `
			| where { -not [System.String]::IsNullOrEmpty($_.Split()[0]) } `
			| % {
				$bn = $_.Split()[1]
				Write-Output "(git:$bn)"
			}
	}
}

# Mercurial 管理ディレクトリ
function hg_branch {
	if (Test-Path .hg) {
		hg branch 2>$null `
			| % {
				Write-Output "(hg:$_)"
			}
	}
}

# プロンプト表示を変更する
function prompt {
	# カレントディレクトリをウィンドウタイトルにする
	(Get-Host).UI.RawUI.WindowTitle = "Windows PowerShell " + $pwd

	$id = 1
	$historyItem = Get-History -Count 1
	if ($historyItem)
	{
		$id = $historyItem.Id + 1
	}

	# GitBashっぽく表示
	# カレントディレクトリを取得
	$idx = $pwd.ProviderPath.LastIndexof("\") + 1
	$cdn = $pwd.ProviderPath.Remove(0, $idx)

	# ブランチ名を取得
	$gitBranch = git_branch
	$hgBranch = hg_branch

	# プロンプトをセット
	Write-Host "[" -NoNewline -ForegroundColor White
	Write-Host "$env:USERNAME@$env:USERDOMAIN" -NoNewline -ForegroundColor Cyan
	Write-Host " $cdn" -NoNewline -ForegroundColor Green
	Write-Host "$gitBranch$hgBranch" -NoNewline -ForegroundColor White
	Write-Host " $id]" -ForegroundColor White
	Write-Host " $" -NoNewline -ForegroundColor White
	return " "
}
