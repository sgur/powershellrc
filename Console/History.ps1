#履歴の保存数を最大に設定
$MaximumHistoryCount = 10240
#履歴の設定
$HistoryFile = Join-Path (Split-Path $Profile -Parent) History.xml
Register-EngineEvent PowerShell.Exiting -SupportEvent -Action {
	Get-History -count $MaximumHistoryCount | Export-Clixml $HistoryFile
}
if (Test-Path $HistoryFile) {
	Import-Clixml $HistoryFile | Add-History
}
