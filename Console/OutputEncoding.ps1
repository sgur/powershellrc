# PowerShell -> Cmd へのパイプ : ShiftJIS (CP932)
if ([Text.Encoding]::Default.CodePage -eq 932) {
	[Console]::OutputEncoding = [Text.Encoding]::Default
} else {
	[Console]::OutputEncoding = [Text.Encoding]::GetEncoding("shift_jis")
}

# PowerShell 内でのパイプラインのエンコーディング : UTF-8
# $OutputEncoding = [Text.Encoding]::GetEncoding("utf-8")
$OutputEncoding = [Text.Encoding]::UTF8
