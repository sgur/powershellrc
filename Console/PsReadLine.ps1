# PSReadLine が入ってたら設定する
if ((Get-Module -ListAvailable -Name PSReadLine).Count -gt 0 -and `
		-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
			[Security.Principal.WindowsBuiltInRole] "Administrator"))
{
	Set-PSReadlineOption -EditMode Emacs
	Set-PSReadlineKeyHandler -Key UpArrow -Function HistorySearchBackward
	Set-PSReadlineKeyHandler -Key DownArrow -Function HistorySearchForward

	Set-PSReadlineOption -AddToHistoryHandler {
		param($Command)
			$LastCommand = $(cat $(Get-PSReadlineOption).HistorySavePath -Tail $([regex]::Matches($Command, '\n').Count + 1) -Encoding UTF8) -join ''
			return $LastCommand -ne ($Command -replace '\n','`')
	}
}

